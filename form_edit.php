<?php 
include('data.php');
$dat = new Data();
if(isset($_GET['id'])){
    $id = $_GET['id']; 
    $data_siswa = $dat->get_by_id($id);
}
else
{
    header('Location: index.php');
}
 
if(isset($_POST['tombol_update'])){
    $id = $_POST['id'];
    $name = $_POST['name'];
    $rarity = $_POST['rarity'];
    $price = $_POST['price']; 
    $status_update = $dat->update($id,$name,$rarity,$price);
    if($status_update)
    {
        header('Location:index.php');
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <title>Edit Data</title>
    </head>
    <body>
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3>Update Data</h3>
            </div>
            <div class="card-body">
            <form method="post" action="">
                <input type="hidden" name="id" value="<?php echo $data_siswa['id']; ?>"/>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="name" value="<?php echo $data_siswa['name']; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="rarity" class="col-sm-2 col-form-label">Rarity</label>
                    <div class="col-sm-10">
                    <input type="text" value="<?php echo $data_siswa['rarity']; ?>" name="rarity" class="form-control" id="rarity" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="price" class="col-sm-2 col-form-label">Price</label>
                    <div class="col-sm-10">
                    <textarea class="form-control" name="price" id="price" required><?php echo $data_siswa['price']; ?></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="salary" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                    <button type="submit" name="tombol_update" class="btn btn-outline-primary"><i class="fas fa-check"></i></button>
                    <a href="index.php" class="btn btn-outline-danger"><i class="fas fa-times"></i></a>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
</body>
</html>