<?php 
include('data.php');

$dat = new Data();
$data_pegawai = $dat->show();
 
if(isset($_GET['delete_pegawai']))
{
    $id = $_GET['delete_pegawai'];
    $delete_status = $dat->delete($id);
    if($delete_status)
    {
        header('Location: index.php');
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Marketplace</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
</head>
<style>
    #image_bg{
        /* The image used */
        background-image: url("image/wave.jpg");

        /* Full height */
        height: 100%;

        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .image_button{
        /* The image used */
        background-image: url("image/galaxy.jpg");

        /* Full height */
        height: 100%;

        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
</style>
<body>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="page-header clearfix">
                <nav class="navbar navbar-dark bg-primary" id="image_bg">
                <h3 class="text-light text-center">In Game Item Marketplace</h3>
                <div class="image_button">
                <a href="form_add.php" class="btn btn-outline-light" title='Add Item' data-toggle='tooltip'><i class="fas fa-cart-plus" ></i></a>
                </div>
                </nav>
            </div>
            <div class="card-body">
                <hr/>
                <table class="table table-bordered" width="60%">
                    <tr>
                        <th>No</th>
                        <th>Name Of Item</th>
                        <th>Rarity</th>
                        <th>Price</th>
                        <th>Tools</th>
                    </tr>
                    <?php 
                    $no = 1;
                    foreach($data_pegawai as $row)
                    {
                        echo "<tr>";
                        echo "<td>".$no."</td>";
                        echo "<td>".$row['name']."</td>";
                        echo "<td>".$row['rarity']."</td>";
                        echo "<td>"."Rp. ".number_format($row['price'])."</td>";
                        echo "<td><a href='form_edit.php?id=".$row['id'] ."' title='Update Record' data-toggle='tooltip'><span class='btn btn-outline-primary glyphicon glyphicon-pencil'></span></a> || <a href='index.php?delete_pegawai=".$row['id'] ."' title='Delete Record' data-toggle='tooltip'><span class='btn btn-outline-danger glyphicon glyphicon-trash'></span></a></td>";
                        echo "</tr>";
                        $no++;
                    }
                    ?>
                </table>
                </div>
            </div>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
</body>
</html>