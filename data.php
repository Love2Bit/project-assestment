<?php
class Data
{
    public function __construct()
    {
        $host = "localhost";
        $dbname = "market";
        $username = "root";
        $password = "";
        $this->db = new PDO("mysql:host={$host};dbname={$dbname}", $username, $password);
    }
    public function add_data($name, $rarity, $price)
    {
        $data = $this->db->prepare('INSERT INTO item (name,rarity,price) VALUES (?, ?, ?)');
        
        $data->bindParam(1, $name);
        $data->bindParam(2, $rarity);
        $data->bindParam(3, $price);
 
        $data->execute();
        return $data->rowCount();
    }
    public function show()
    {
        $query = $this->db->prepare("SELECT * FROM item");
        $query->execute();
        $data = $query->fetchAll();
        return $data;
    }
 
    public function get_by_id($id){
        $query = $this->db->prepare("SELECT * FROM item where id=?");
        $query->bindParam(1, $id);
        $query->execute();
        return $query->fetch();
    }
 
    public function update($id,$name,$rarity,$price){
        $query = $this->db->prepare('UPDATE item set name=?,rarity=?,price=? where id=?');
        
        $query->bindParam(1, $name);
        $query->bindParam(2, $rarity);
        $query->bindParam(3, $price);
        $query->bindParam(4, $id);
 
        $query->execute();
        return $query->rowCount();
    }
 
    public function delete($id)
    {
        $query = $this->db->prepare("DELETE FROM item where id=?");
 
        $query->bindParam(1, $id);
 
        $query->execute();
        return $query->rowCount();
    }
 
}
?>